# Buslane

A universal request library backed by [Ky](https://github.com/sindresorhus/ky) with an eventbus.

### Why wrap Ky?
Ky is great for handling the low-level requests, but sometimes we want to trigger events in response to general request errors.
That is where Buslane comes in, it allows you to dispatch events inside your requests, and then handle them in a central location.

A good example is updating a user's authentication state after a 401.


### Initialization
```typescript
const buslane = new Buslane({
    prefixUrl: '/',
    hooks: {
        beforeRequest: [],
        afterResponse: [
            async (request, options, response) => {
                if (response.status === 401) {
                    await buslane.emitAsync('401');
                } else if (response.status === 403) {
                    await buslane.emitAsync('403');
                }
            },
        ],
    },
});
```

You can start using Buslane directly, or inside a React app you can provide access to the Buslane instance through a provider

```typescript
    <BuslaneProvider buslane={buslane}
		<MyApp />
    </BuslaneProvider>
```