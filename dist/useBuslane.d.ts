import React from 'react';
import Buslane from './Buslane';
interface BuslaneProviderProps {
    buslane: Buslane;
}
export declare const BuslaneProvider: React.FunctionComponent<BuslaneProviderProps>;
export declare const useBuslane: () => Buslane;
export {};
