import { Options, Input, ResponsePromise } from 'ky';
import ky from 'ky-universal';
import EventEmitter from 'eventemitter3';
export default class Buslane extends EventEmitter {
    client: typeof ky;
    constructor(defaultOptions?: Options);
    get(url: Input, options?: Options): ResponsePromise;
    post(url: Input, options?: Options): ResponsePromise;
    put(url: Input, options?: Options): ResponsePromise;
    patch(url: Input, options?: Options): ResponsePromise;
    head(url: Input, options?: Options): ResponsePromise;
    delete(url: Input, options?: Options): ResponsePromise;
    emitAsync(event: any, ...args: any): Promise<boolean>;
}
