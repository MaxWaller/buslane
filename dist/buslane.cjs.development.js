'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var ky = _interopDefault(require('ky-universal'));
var EventEmitter = _interopDefault(require('eventemitter3'));
var React = require('react');
var React__default = _interopDefault(React);

function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;
  subClass.__proto__ = superClass;
}

// A type of promise-like that resolves synchronously and supports only one observer

const _iteratorSymbol = /*#__PURE__*/ typeof Symbol !== "undefined" ? (Symbol.iterator || (Symbol.iterator = Symbol("Symbol.iterator"))) : "@@iterator";

const _asyncIteratorSymbol = /*#__PURE__*/ typeof Symbol !== "undefined" ? (Symbol.asyncIterator || (Symbol.asyncIterator = Symbol("Symbol.asyncIterator"))) : "@@asyncIterator";

// Asynchronously call a function and send errors to recovery continuation
function _catch(body, recover) {
	try {
		var result = body();
	} catch(e) {
		return recover(e);
	}
	if (result && result.then) {
		return result.then(void 0, recover);
	}
	return result;
}

var Buslane = /*#__PURE__*/function (_EventEmitter) {
  _inheritsLoose(Buslane, _EventEmitter);

  function Buslane(defaultOptions) {
    var _this;

    if (defaultOptions === void 0) {
      defaultOptions = {};
    }

    _this = _EventEmitter.call(this) || this;
    _this.client = ky.create(defaultOptions);
    return _this;
  }

  var _proto = Buslane.prototype;

  _proto.get = function get(url, options) {
    return this.client.get(url, options);
  };

  _proto.post = function post(url, options) {
    return this.client.post(url, options);
  };

  _proto.put = function put(url, options) {
    return this.client.put(url, options);
  };

  _proto.patch = function patch(url, options) {
    return this.client.patch(url, options);
  };

  _proto.head = function head(url, options) {
    return this.client.head(url, options);
  };

  _proto["delete"] = function _delete(url, options) {
    return this.client["delete"](url, options);
  };

  _proto.emitAsync = function emitAsync(event) {
    for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    try {
      var _this3 = this;

      var eventId = EventEmitter.prefixed ? EventEmitter.prefixed + event : event; // @ts-ignore

      if (!_this3._events[eventId]) return Promise.resolve(false); // @ts-ignore

      var listeners = _this3._events[eventId];

      var _temp3 = function () {
        if (listeners.fn) {
          if (listeners.once) _this3.removeListener(event, listeners.fn, undefined, true);

          var _temp4 = _catch(function () {
            var _listeners$fn;

            return Promise.resolve((_listeners$fn = listeners.fn).call.apply(_listeners$fn, [listeners.context].concat(args))).then(function () {});
          }, function () {});

          if (_temp4 && _temp4.then) return _temp4.then(function () {});
        } else {
          return Promise.resolve(Promise.all(listeners.map(function (listener) {
            try {
              if (listener.once) {
                _this3.removeListener(event, listener.fn, undefined, true);
              }

              var _temp6 = _catch(function () {
                var _listeners$fn2;

                return Promise.resolve((_listeners$fn2 = listeners.fn).call.apply(_listeners$fn2, [listeners.context].concat(args))).then(function () {});
              }, function () {});

              return Promise.resolve(_temp6 && _temp6.then ? _temp6.then(function () {}) : void 0);
            } catch (e) {
              return Promise.reject(e);
            }
          }))).then(function () {});
        }
      }();

      return Promise.resolve(_temp3 && _temp3.then ? _temp3.then(function () {
        return true;
      }) : true);
    } catch (e) {
      return Promise.reject(e);
    }
  };

  return Buslane;
}(EventEmitter);

var BuslaneContext = /*#__PURE__*/React.createContext(undefined);
var BuslaneProvider = function BuslaneProvider(_ref) {
  var buslane = _ref.buslane,
      children = _ref.children;
  return React__default.createElement(BuslaneContext.Provider, {
    value: buslane
  }, children);
};
var useBuslane = function useBuslane() {
  return React.useContext(BuslaneContext);
};

exports.Buslane = Buslane;
exports.BuslaneProvider = BuslaneProvider;
exports.useBuslane = useBuslane;
//# sourceMappingURL=buslane.cjs.development.js.map
