import React, { createContext, useContext } from 'react';
import Buslane from './Buslane';

// @ts-ignore
const BuslaneContext = createContext<Buslane>(undefined);

interface BuslaneProviderProps {
  buslane: Buslane;
}

export const BuslaneProvider: React.FunctionComponent<BuslaneProviderProps> = ({
  buslane,
  children,
}) => {
  return (
    <BuslaneContext.Provider value={buslane}>
      {children}
    </BuslaneContext.Provider>
  );
};

export const useBuslane: () => Buslane = () => {
  return useContext(BuslaneContext);
};
