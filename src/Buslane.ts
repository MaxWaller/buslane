import { Options, Input, ResponsePromise } from 'ky';
import ky from 'ky-universal';
import EventEmitter from 'eventemitter3';

export default class Buslane extends EventEmitter {
  public client: typeof ky;

  public constructor(defaultOptions: Options = {}) {
    super();
    this.client = ky.create(defaultOptions);
  }

  public get(url: Input, options?: Options): ResponsePromise {
    return this.client.get(url, options);
  }
  public post(url: Input, options?: Options): ResponsePromise {
    return this.client.post(url, options);
  }
  public put(url: Input, options?: Options): ResponsePromise {
    return this.client.put(url, options);
  }
  public patch(url: Input, options?: Options): ResponsePromise {
    return this.client.patch(url, options);
  }
  public head(url: Input, options?: Options): ResponsePromise {
    return this.client.head(url, options);
  }
  public delete(url: Input, options?: Options): ResponsePromise {
    return this.client.delete(url, options);
  }

  public async emitAsync(event: any, ...args: any) {
    let eventId = EventEmitter.prefixed ? EventEmitter.prefixed + event : event;
    // @ts-ignore
    if (!this._events[eventId]) return false;
    // @ts-ignore
    let listeners = this._events[eventId];
    if (listeners.fn) {
      if (listeners.once)
        this.removeListener(event, listeners.fn, undefined, true);
      try {
        await listeners.fn.call(listeners.context, ...args);
      } catch (e) {
        // silence is bliss
      }
    } else {
      await Promise.all(
        listeners.map(async (listener: any) => {
          if (listener.once) {
            this.removeListener(event, listener.fn, undefined, true);
          }
          try {
            await listeners.fn.call(listeners.context, ...args);
          } catch (e) {
            // silence is bliss
          }
        })
      );
    }
    return true;
  }
}
